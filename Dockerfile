# Use an official Debian runtime as a parent image
FROM debian:latest


# Set proxy server, replace host:port with values for your servers
#ENV http_proxy http://192.168.5.5:3128
#ENV https_proxy http://192.168.5.5:3128
ENV DEBIAN_FRONTEND noninteractive

# Install any needed packages specified in requirements.txt
#RUN pip install --trusted-host pypi.python.org -r requirements.txt

RUN apt-get update
RUN apt-get -y install apt-utils
RUN apt-get -y dist-upgrade
RUN apt-get -y install git

# for testing net-tools joe
RUN apt-get -y install net-tools joe aptitude curl gpg
RUN apt-get -y install build-essential
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs
RUN cd /; git clone git://github.com/askmike/gekko.git -b stable
RUN cd gekko; npm install --only=production
#RUN npm install -g grunt-cli grunt-init bower

# Make port 80 available to the world outside this container
#EXPOSE 4000

# Define environment variable
#ENV NAME World

# Set the working directory to /app
#WORKDIR /app

# Copy the current directory contents into the container at /app
#ADD . /app

# Run when the container launches
CMD ["bash"]

